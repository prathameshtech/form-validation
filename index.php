<?php
include 'connection.php';

?>

<!doctype html>
<html>
    <head>
        <title>Form</title> 
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.0.2/css/bootstrap.min.css" >
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="js.js"></script>
        <link rel="stylesheet" href="style.css">
    </head>
    <Body>
    <form id="formValidation"  method="post" action ="" enctype='multipart/form-data'>
            <h4>Form Validation</h4>
            <div class="row">
              <div class="col-md-12">
                <label class="form-label">Email</label>
                <input type="text" name="email" id="email" class="form-control" >
                <span id="Erremail"><?php echo $Erremail ?></span>
              </div>
              <div class="col-md-12">
                <label class="form-label">Name</label>
                <input type="text" name="name" id="name" class="form-control">
                <span id="Errname"><?php echo $Errname ?></span>
              </div>
              <div class="col-md-12">
                <label class="form-label">Mobile</label>
                <input type="number" name="mobile" id="mobile" class="form-control" >
                <span id="Errmobile"><?php echo $Errmobile ?></span>
              </div>
              <div class="col-md-12">
                <label class="form-label">Password</label>
                <input type="password" name="password" id="password" class="form-control"><i class="fa fa-eye" id="toggle1" aria-hidden="true"></i>
                <span id="Errpass"><?php echo $Errpass ?></span>
              </div> 
              <div class="col-md-12">
                <label class="form-label">Confirm Password</label>
                <input type="password" name="con-password" id="con-password" class="form-control" ><i class="fa fa-eye" id="toggle2" aria-hidden="true"></i>
                <span id="Errcon_pass"><?php echo $Errcon_pass ?></span>
              </div> 
              <div class="col-md-12">
                <label>Image upload</label>
                <input type="file" name="files[]" multiple> 
                <span id="Errimage"><?php echo $Errimage ?></span>
              </div>
              <div class="col-md-12 button">
                <button type="submit" name="submit" value="submit">submit</button>
              </div>
              </div>
            </form>

           
    </body>
</html>

